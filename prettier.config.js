//** @type {import("prettier").Config} */
const config = {
  trailingComma: 'es5',
  tabWidth: 4,
  singleQuote: true,
  semi: true,
};

export default config;

