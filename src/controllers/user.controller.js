import User from '../models/user.model';
import {
    hashPassword,
    verifyPassword,
    generateToken,
    verifyToken,
} from '../helpers.js';

export const createUser = async (req, res) => {
    try {
        const { email, username, pass } = req.body;
        const hashedPassword = hashPassword(pass);
        const result = await User.create({
            username,
            email,
            password: hashedPassword,
        });
        res.status(201).json(result._id);
    } catch (err) {
        res.status(500).json(err);
    }
};

export const loginUser = async (req, res) => {
    try {
        const { email, pass } = req.body;
        const { password, username, _id } = await User.findOne({ email });
        const verified = verifyPassword(password, pass);
        if (verified) {
            const token = generateToken({ username, email, id: _id });
            res.cookie('jwt', token, { httpOnly: true });
            res.status(200).json({ message: 'Login Successful' });
        } else {
            res.status(401).json('Unauthorized');
        }
    } catch (error) {
        res.status(401).json(error);
    }
};

export const authenticateUser = async (req, res) => {
    const token = req.cookies.jwt;
    if (!token) {
        return res.status(403).json('Forbidden');
    }
    try {
        const decoded = verifyToken(token);
        res.status(200).json(decoded);
    } catch (error) {
        res.status(401).json(error);
    }
};
