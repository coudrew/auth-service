import express from 'express';
import cookieParser from 'cookie-parser';
import user from './routes/user.route';

const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(user);
app.get('/health', (req, res) => {
    res.status(200).json({ active: true });
});

export default app;
