import express from 'express';
import {
    createUser,
    loginUser,
    authenticateUser,
} from '../controllers/user.controller.js';

const router = express.Router();

router.post('/user', createUser);
router.post('/login', loginUser);
router.post('/auth', authenticateUser);

export default router;