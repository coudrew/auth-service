import 'dotenv/config';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const { JWT_SECRET } = process.env;
const saltRounds = 13;
export const hashPassword = async (password) => {
    try {
        const salt = await bcrypt.genSalt(saltRounds);
        const hashedPassword = await bcrypt.hash(password, salt);
        return hashedPassword;
    } catch (error) {
        throw new Error('Error hashing password');
    }
};

export const verifyPassword = async (hashedPassword, password) => {
    try {
        const result = await bcrypt.compare(hashedPassword, password);
        return result;
    } catch (error) {
        throw new Error('Password not verified');
    }
};

export const generateToken = async (data) => {
    if (!JWT_SECRET) {
        throw new Error('Secret Missing');
    }
    try {
        const token = await jwt.sign({ data }, JWT_SECRET, {
            expiresIn: '23h',
        });
        return token;
    } catch (error) {
        throw new Error('Token generation failed');
    }
};

export const verifyToken = async (token) => {
    if (!JWT_SECRET) {
        throw new Error('Secret Missing');
    }
    try {
        const decoded = await jwt.verify(token, JWT_SECRET);
        return decoded;
    } catch (error) {
        throw new Error('Token verification failed');
    }
};
