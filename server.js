import 'dotenv/config';
import mongoose from 'mongoose';
import app from './src/app.js';

const { MONGODB_URI, PORT, MONGODB_USER, MONGODB_USER_PASS } = process.env;

mongoose
    .connect(MONGODB_URI, {
        user: MONGODB_USER,
        pass: MONGODB_USER_PASS,
    })
    .then(() => console.log('Conntected to MongoDB'))
    .then(() =>
        app.listen(PORT, () => console.log(`Server listening on port ${PORT}`))
    )
    .catch((err) => console.log(err));
