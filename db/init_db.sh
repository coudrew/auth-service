#!/bin/sh

DATABASE="auth"
COLLECTION="users"

mongosh $DATABASE --eval "db.createUser({user: '$MONGODB_USER', pwd: '$MONGODB_USER_PASS', roles: [{role: 'readWrite', db: 'doc-service'}]})"
# mongosh $DATABASE --eval "db.$COLLECTION.insertOne($DATA)"