FROM mongo:latest
ARG mongodb_user
ARG mongodb_user_pass
ENV MONGO_INITDB_DATABASE=auth
ENV MONGODB_USER=$mongodb_user
ENV MONGODB_USER_PASS=$mongodb_user_pass

COPY init_db.sh /docker-entrypoint-initdb.d/

EXPOSE 27017

CMD ["mongod"]